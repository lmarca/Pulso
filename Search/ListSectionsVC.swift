//
//  ListSectionsVC.swift
//  Pulso
//
//  Created by Procesos on 5/12/16.
//  Copyright © 2016 Luis Marca Zapata. All rights reserved.
//

import UIKit

class ListSectionsVC: UIViewController {
    
    @IBOutlet weak var heightSectionThree: NSLayoutConstraint!
    
    @IBOutlet weak var heightSectionTwo: NSLayoutConstraint!
    
    @IBOutlet weak var heightSectionOne: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()
       
        

        // Do any additional setup after loading the view.
    }
    
    func calculoMedidas() {
        if UIDevice().userInterfaceIdiom == .Phone {
            switch UIScreen.mainScreen().nativeBounds.height {
            case 960:
                print("iPhone 4 or 4S")
            case 1136:
                print("iPhone 5 or 5S or 5C")
                heightSectionOne.constant = 132
                heightSectionTwo.constant = 133
                heightSectionThree.constant = 133
            case 1334:
                print("iPhone 6 or 6S")
            case 2208:
                print("iPhone 6+ or 6S+")
            default:
                print("unknown")
                heightSectionOne.constant = 170
                heightSectionTwo.constant = 170
                heightSectionThree.constant = 170
            }
        }
    }
    
    @IBAction func onClickEvent(sender: AnyObject) {
        
        let vc = self.storyboard!.instantiateViewControllerWithIdentifier("Filiacion01VC") as! Filiacion01VC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
