//
//  ListRiesgosVC.swift
//  Pulso
//
//  Created by Procesos on 8/12/16.
//  Copyright © 2016 Luis Marca Zapata. All rights reserved.
//

import UIKit

class ListRiesgosVC: UIViewController {
    
    var eventSections = ["", ""]
    let reuseIdentifier:String = "CustomRiesgosCell"
    
    @IBOutlet weak var collectionView: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.automaticallyAdjustsScrollViewInsets = false
        self.edgesForExtendedLayout = .None
        
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumInteritemSpacing = 1
        layout.minimumLineSpacing = 1
         
        layout.itemSize = CGSize(width: 340, height: 165)
        layout.scrollDirection = .Horizontal
         
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
         
        collectionView.collectionViewLayout = layout
        collectionView!.dataSource = self
        collectionView!.delegate = self
        collectionView.registerNib(UINib(nibName: "CustomRiesgosCell", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
        collectionView!.backgroundColor = UIColor.clearColor()
         
        collectionView.reloadData()
 

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ListRiesgosVC : UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    // MARK: - CollectionView data source
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return eventSections.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! CustomRiesgosCell
        
        //cell.tittle.text = eventSections[indexPath.item] as? String
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath){
        
    }
    
    
}

