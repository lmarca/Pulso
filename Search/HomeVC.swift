//
//  HomeVC.swift
//  Pulso
//
//  Created by Procesos on 16/11/16.
//  Copyright © 2016 Luis Marca Zapata. All rights reserved.
//

import UIKit

class HomeVC: myActivity, UINavigationControllerDelegate {
    

    var eventListViewController: ListResultsVC?
    var eventCollectViewController: ListRiesgosVC?
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var company: UILabel!
    
    @IBOutlet weak var profileImageView: ProfileImageView!
    var takenImage: UIImage?
    
    @IBOutlet weak var choosePhoto: UIButton!
    @IBOutlet weak var btnUbicanos: UIButton!
    @IBOutlet weak var btnResultados: UIButton!
    @IBOutlet weak var btnRiesgos: UIButton!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        profileImageView.image = readImage()
    
        let menuButton = UIBarButtonItem(image: UIImage(named: "iconHospital")?.imageWithRenderingMode(.AlwaysOriginal), style: .Plain, target: revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)))
        navigationItem.leftBarButtonItem = menuButton
        
        let ovalButton = UIBarButtonItem(image: UIImage(named: "iconOval")?.imageWithRenderingMode(.AlwaysOriginal), style: .Plain, target: revealViewController(), action: nil)
        navigationItem.rightBarButtonItem = ovalButton
        
        
        let button = UIButton(type: .Custom)
        button .setImage(UIImage(named: "iconCardio"), forState: .Normal)
        button.frame = CGRectMake(0, 0, 170, 35)
        self.navigationItem.titleView = button
        
        choosePhoto.addTarget(self, action: #selector(HomeVC.choosePhotoSource), forControlEvents: UIControlEvents.TouchUpInside)
        
        createEventTableView()
        
        if UserManager.hasCurrentUser() {
            name.text = UserManager.sharedInstance.currentUser?.displayName()
            company.text = UserManager.sharedInstance.currentUser?.residenciaDireccion
        }
        
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func onClickEvent(sender: AnyObject) {
        
        switch sender .tag {
        case 0:
            let loginViewController = self.storyboard!.instantiateViewControllerWithIdentifier("UbicanosVC") as! UbicanosVC
            loginViewController.isDismiss = true
            let navigationViewController = self.storyboard!.instantiateViewControllerWithIdentifier("UbicanosNavController") as! BlackTransparentNavigationController
            navigationViewController.setViewControllers([loginViewController], animated: true)
            self.showViewController(navigationViewController, sender: self)
            break
        case 1:
            let loginViewController = self.storyboard!.instantiateViewControllerWithIdentifier("ListResultadosVC") as! ListResultadosVC
            loginViewController.isDismiss = true
            let navigationViewController = self.storyboard!.instantiateViewControllerWithIdentifier("ResultadosNavController") as! BlackTransparentNavigationController
            navigationViewController.setViewControllers([loginViewController], animated: true)
            self.showViewController(navigationViewController, sender: self)
            break
        case 2:
            createEventCollectionView()
            break
        default:
            break
        }
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func createEventCollectionView() {
        
        for item in self.childViewControllers {
            item.willMoveToParentViewController(nil)
            item.view.removeFromSuperview()
            item.removeFromParentViewController()
        }
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        eventCollectViewController = storyboard.instantiateViewControllerWithIdentifier("ListRiesgosVC") as?  ListRiesgosVC
        
        let eventListViewControllerView = eventCollectViewController!.view
        self.addChildViewController(eventCollectViewController!)
        eventCollectViewController!.view.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(eventListViewControllerView)
        eventCollectViewController!.didMoveToParentViewController(self)
        //Then you put the constraints
        
        let horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-(0)-[eventListViewControllerView]-(0)-|", options: NSLayoutFormatOptions(rawValue: 0), metrics:nil , views: ["eventListViewControllerView": eventListViewControllerView])
        
        let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-(460)-[eventListViewControllerView]-(0)-|", options: NSLayoutFormatOptions(rawValue: 0), metrics:nil , views: ["eventListViewControllerView": eventListViewControllerView])
        
        NSLayoutConstraint.activateConstraints(verticalConstraints)
        NSLayoutConstraint.activateConstraints(horizontalConstraints)
        
    }
    
    func createEventTableView() {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        eventListViewController = storyboard.instantiateViewControllerWithIdentifier("ListResultsVC") as?  ListResultsVC
        
        let eventListViewControllerView = eventListViewController!.view
        self.addChildViewController(eventListViewController!)
        eventListViewController!.view.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(eventListViewControllerView)
        eventListViewController!.didMoveToParentViewController(self)
        //Then you put the constraints
        
        let horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-(0)-[eventListViewControllerView]-(0)-|", options: NSLayoutFormatOptions(rawValue: 0), metrics:nil , views: ["eventListViewControllerView": eventListViewControllerView])
        
        let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-(460)-[eventListViewControllerView]-(0)-|", options: NSLayoutFormatOptions(rawValue: 0), metrics:nil , views: ["eventListViewControllerView": eventListViewControllerView])
        
        NSLayoutConstraint.activateConstraints(verticalConstraints)
        NSLayoutConstraint.activateConstraints(horizontalConstraints)
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


//MARK: Photo Image
extension HomeVC {
    
    func choosePhotoSource() {
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        
        let chooseFromLibrary = UIAlertAction(title: "Escoger de la fototeca", style: .Default)  { (action) in
            
            NSOperationQueue.mainQueue().addOperationWithBlock({
                self.initializeImagePickerController(.PhotoLibrary)
            })
        }
        
        alertController.addAction(chooseFromLibrary)
        
        
        let takePhoto = UIAlertAction(title: "Tomar con la cámara", style: .Default)  { (action) in
            
            NSOperationQueue.mainQueue().addOperationWithBlock({
                self.initializeImagePickerController(.Camera)
            })
        }
        
        alertController.addAction(takePhoto)
        
        let cancel = UIAlertAction(title: "Cancelar", style: .Cancel, handler:nil)
        alertController.addAction(cancel)
        
        self.presentViewController(alertController, animated:true, completion: nil)
    }
    
    func initializeImagePickerController(sourceType: UIImagePickerControllerSourceType) {
        
        let picker = UIImagePickerController()
        picker.allowsEditing = true
        picker.delegate = self
        picker.sourceType = sourceType
        presentViewController(picker, animated: true, completion: nil)
    }
}

extension HomeVC: UIImagePickerControllerDelegate {
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        var newImage: UIImage
        
        if let possibleImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            newImage = possibleImage
        }
            
        else if let possibleImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            newImage = possibleImage
        }
            
        else {
            return
        }
        
        saveImage()
        takenImage = newImage
        profileImageView.image = takenImage
        
        dismissViewControllerAnimated(true, completion: nil)
    }
}


extension HomeVC {
    
    func saveImage() {
        
        dispatch_async(dispatch_get_main_queue()) {
            // update some UI
            if let image = self.takenImage {
                if let data = UIImagePNGRepresentation(image) {
                    let filename = self.getDocumentsDirectory().URLByAppendingPathComponent("UserIcon.png")
                    data.writeToURL(filename!, atomically: true)
                }
            }
        }
    }
    
    func readImage() -> UIImage {
        
        let filePath = getDocumentsDirectory().URLByAppendingPathComponent("UserIcon.png")?.path
        if NSFileManager.defaultManager().fileExistsAtPath(filePath!) {
            return UIImage(contentsOfFile: filePath!)!
        }
        
        return UIImage()
    }
    
    func getDocumentsDirectory() -> NSURL {
        
        let paths = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
        
}
