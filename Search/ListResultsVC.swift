
//
//  ListResultsVC.swift
//  Pulso
//
//  Created by Procesos on 8/12/16.
//  Copyright © 2016 Luis Marca Zapata. All rights reserved.
//

import UIKit

class ListResultsVC: myActivity {
    
    var currentResults = [Resultado]()
    let cellIdentifier = "CustomResultsCell"
    
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.automaticallyAdjustsScrollViewInsets = false
        self.edgesForExtendedLayout = .None
        
        PulsoServices.getResultados { (resultados, error) in
            

            NSOperationQueue.mainQueue().addOperationWithBlock(){
                //Do UI stuff here
                self.currentResults = resultados!
                self.tableView.reloadData()
            }
        }
        

        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ListResultsVC: UITableViewDataSource {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentResults.count
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! CustomResultsCell
        
        cell.selectionStyle = .None
        tableView.separatorStyle = .None
        
        let ley = currentResults[indexPath.row]
        
        if ley.status == "PENDIENTE" {
            cell.shape.image = UIImage(named: "shapeRed")
        }else{
            cell.shape.image = UIImage(named: "shapeGreen")
        }
        
        cell.name.text = ley.name
        cell.status.text = ley.status
        
        return cell
    }
    
}
