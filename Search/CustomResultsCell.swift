//
//  CustomResultsCell.swift
//  Pulso
//
//  Created by Procesos on 8/12/16.
//  Copyright © 2016 Luis Marca Zapata. All rights reserved.
//

import UIKit

class CustomResultsCell: UITableViewCell {
    
    @IBOutlet weak var shape: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var status: UILabel!
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backgroundColor = UIColor.clearColor()
        self.status.font = UIFont(name: "Abadi MT Condensed Extra Bold", size: 14)
        // Initialization code
    }

    override func setSelected( selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
