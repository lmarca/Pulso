//
//  PulsoServices.swift
//  Pulso
//
//  Created by Procesos on 8/12/16.
//  Copyright © 2016 Luis Marca Zapata. All rights reserved.
//

import UIKit


class PulsoServices {
    
    static let allResultadosPath = "resultados/16242"
    
    static let loginPath = "login/"
    
    static let rankingPath = "rank"
    

    static func sendRanking(rank:String, content:String, completionHandler:(msg: String!, error:NSError?) -> Void) {
        
        //let token = (UserManager.sharedInstance.currentUser?.numDoc)!
        
        let request = RequestBuilder.POST(path: rankingPath, body: ["idpaciente": "16242", "rank": rank, "content": content])
        
        NetworkManager().dataTaskWithRequest(request) { (jsonObject, error) -> Void in
            
            if (error != nil) {
                completionHandler(msg: nil, error: error)
            }
                
            else {
                
                if let jsonDictionary = jsonObject as? [String:AnyObject]  {
                    
                    completionHandler(msg: jsonDictionary["message"] as? String, error: nil)
                }
                    
                else {
                    completionHandler(msg: nil, error: NSError.defaultError())
                }
            }
        }
    }
    
    
    static func login(email:String, password:String, completionHandler:(user: User!, error:NSError?) -> Void) {
        
        let request = RequestBuilder.GET(path: loginPath + email + "/" + password)
        
        NetworkManager().dataTaskWithRequest(request) { (jsonObject, error) -> Void in
            
            if (error != nil) {
                completionHandler(user: nil, error: error)
            }
                
            else {
                
                if let jsonDictionary = jsonObject as? [String:AnyObject]  {
                    
                    let user = User(jsonObject: jsonDictionary)
                    completionHandler(user: user, error: nil)
                }
                    
                else {
                    completionHandler(user: nil, error: NSError.defaultError())
                }
            }
        }
    }
    
    
    static func getResultados(completionHandler:(resultados: [Resultado]?, error:NSError?) -> Void) {
        
        var resultados = [Resultado]()
        
        let request = RequestBuilder.GET(path: allResultadosPath)
        
        
        NetworkManager().dataTaskWithRequest(request) { (jsonObject, error) -> Void in
            
            if (error != nil) {
                print(error?.description)
                completionHandler(resultados:nil, error: error)
            }
                
            else {
                
                if let jsonDictionary = jsonObject as? [String:AnyObject]  {
                    
                    for (key, value) in jsonDictionary {
                        print("key \(key) value \(value)")
                        resultados.append(Resultado(name: key, status: value as! String))
                    }
                    
                    completionHandler(resultados:resultados, error: nil)
                }
                else {
                    completionHandler(resultados:nil, error: NSError.errorWithMessage("Error"))
                }
            }
        }
    }
    
    /*
    static func getAllLeyes( completionHandler:([Ley]?, error:NSError?) -> Void) {
        
        let request = RequestBuilder.GET(path: "")
        
        NetworkManager().dataTaskWithRequest(request) { (jsonObject, error) -> Void in
            
            var objects = [Ley]()
            
            if (error != nil) {
                completionHandler(nil, error: error)
            }
                
            else {
                
                if let responseDictionary = jsonObject as? [String:AnyObject]  {
                    
                    if let jsonObjectArray = responseDictionary["laws"] as? NSArray {
                        
                        print(jsonObjectArray)
                        
                        //Ley.deleteAll()
                        
                        for ley in (jsonObjectArray as! [[String: AnyObject]]) {
                            
                            Ley.saveLey(!(ley["casefile"] is NSNull) ? ley["casefile"] as! String : "", code: !(ley["code"] is NSNull) ? ley["code"] as! String : "", emission_date: !(ley["emission_date"] is NSNull) ? ley["emission_date"] as! String : "", name: !(ley["name"] is NSNull) ? ley["name"] as! String : "", number: !(ley["number"] is NSNull) ? ley["number"] as! String : "", observations: !(ley["observations"] is NSNull) ? ley["observations"] as! String : "", promulgation_date: !(ley["promulgation_date"] is NSNull) ? ley["promulgation_date"] as! String : "", publication_date: !(ley["publication_date"] is NSNull) ? ley["publication_date"] as! String : "", type: !(ley["type"] is NSNull) ? ley["type"] as! String : "", state: "2")
                            
                        }
                        
                        let predicate = NSPredicate(format: "state = 2")
                        
                        let result = DataManager.getManagedObjectsFromEntity(Ley.entityDescription(), predicate: predicate)
                        
                        if result.sucess {
                            for law in result.objects as! [Ley]{
                                objects.append(law)
                            }
                            
                            completionHandler(objects, error: nil)
                        }
                        
                    }
                }
            }
        }
    }
    
    static func getAllLeyesFilter(filter:String, completionHandler:(laws:[Ley]?, error:NSError?) -> Void) {
        
        let request = RequestBuilder.GET(path: "?" + filter)
        
        NetworkManager().dataTaskWithRequest(request) { (jsonObject, error) -> Void in
            
            var objects = [Ley]()
            
            if (error != nil) {
                completionHandler(laws: nil, error: error)
            }
                
            else {
                
                if let responseDictionary = jsonObject as? [String:AnyObject]  {
                    
                    if let jsonObjectArray = responseDictionary["laws"] as? NSArray {
                        
                        Ley.deleteAll()
                        
                        for ley in (jsonObjectArray as! [[String: AnyObject]]) {
                            
                            Ley.saveLey(!(ley["casefile"] is NSNull) ? ley["casefile"] as! String : "", code: !(ley["code"] is NSNull) ? ley["code"] as! String : "", emission_date: !(ley["emission_date"] is NSNull) ? ley["emission_date"] as! String : "", name: !(ley["name"] is NSNull) ? ley["name"] as! String : "", number: !(ley["number"] is NSNull) ? ley["number"] as! String : "", observations: !(ley["observations"] is NSNull) ? ley["observations"] as! String : "", promulgation_date: !(ley["promulgation_date"] is NSNull) ? ley["promulgation_date"] as! String : "", publication_date: !(ley["publication_date"] is NSNull) ? ley["publication_date"] as! String : "", type: !(ley["type"] is NSNull) ? ley["type"] as! String : "", state: "1")
                            
                        }
                        
                        let predicate = NSPredicate(format: "state = 1")
                        
                        let result = DataManager.getManagedObjectsFromEntity(Ley.entityDescription(), predicate: predicate)
                        
                        //let ley = DataManager.getAllManagedObjectsFromEntity(Ley.entityDescription())
                        if result.sucess {
                            for law in result.objects as! [Ley]{
                                objects.append(law)
                            }
                            
                            completionHandler(laws: objects, error: error)
                        }
                        
                    }
                }
            }
        }
    }
    */
    
}
