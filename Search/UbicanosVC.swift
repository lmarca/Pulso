//
//  UbicanosVC.swift
//  Nao
//
//  Created by Procesos on 17/11/16.
//  Copyright © 2016 peru. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation

class UbicanosVC: myActivity {
    
    var eventSections = ["", ""]
    @IBOutlet weak var mapView: GMSMapView!
    var locationManager: CLLocationManager!
    var lastLocation : CLLocation?
    var isDismiss   = false
    
    @IBOutlet var collectionView: UICollectionView!
    let reuseIdentifier:String = "CollectionViewCellLocales"


    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController!.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "Abadi MT Condensed Extra Bold", size: 17)!, NSForegroundColorAttributeName: UIColor.whiteColor()]
        
        self.navigationItem.title = "PulsoSalud"
        
        if !isDismiss {
            let menuButton = UIBarButtonItem(image: UIImage(named: "BackIcon")?.imageWithRenderingMode(.AlwaysOriginal), style: .Plain, target: revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)))
            navigationItem.leftBarButtonItem = menuButton
        }else {
            let menuButton = UIBarButtonItem(image: UIImage(named: "BackIcon")?.imageWithRenderingMode(.AlwaysOriginal), style: .Plain, target: self, action: #selector(UbicanosVC.popToViewController))
            navigationItem.leftBarButtonItem = menuButton
        }
        
        
        mapView.delegate = self
        mapView.settings.rotateGestures = false
        mapView.settings.tiltGestures = false
        mapView.myLocationEnabled = false
        
        mapView.camera = GMSCameraPosition(target: CLLocationCoordinate2D(latitude: -12.0871923, longitude:-76.9951559), zoom: 15, bearing: 0, viewingAngle: 0)
      
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        
        checkLocationAuthorizationStatus()
        
        self.automaticallyAdjustsScrollViewInsets = false
        self.edgesForExtendedLayout = .None
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumInteritemSpacing = 1
        layout.minimumLineSpacing = 1
        
        layout.itemSize = CGSize(width: 340, height: 155)
        layout.scrollDirection = .Horizontal
        
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        
        collectionView.collectionViewLayout = layout
        collectionView!.dataSource = self
        collectionView!.delegate = self
        collectionView.registerNib(UINib(nibName: "LocalCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
        collectionView!.backgroundColor = UIColor.clearColor()
        
        collectionView.reloadData()
        
        let locationPin = GMSMarker()
        locationPin.position = CLLocationCoordinate2DMake(-12.0871923, -76.9951559)
        locationPin.map = mapView
        locationPin.icon = UIImage(named: "locationPin")
        locationPin.userData = ["marker_id": -999]
        
        let locationPin2 = GMSMarker()
        locationPin2.position = CLLocationCoordinate2DMake(-12.0690213, -77.0013786)
        locationPin2.map = mapView
        locationPin2.icon = UIImage(named: "locationPin")
        locationPin2.userData = ["marker_id": -999]

        showGoogleMapsLogo()
        // Do any additional setup after loading the view.
    }
    
     func popToViewController() {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func showGoogleMapsLogo() {
        let mapInsets = UIEdgeInsetsMake(0.0, 0.0, 150.0, 0.0);
        mapView.padding = mapInsets;
    }
    
    func checkLocationAuthorizationStatus() {
        
        switch (CLLocationManager.authorizationStatus()) {
            
        case .NotDetermined:
            locationManager.requestAlwaysAuthorization()
            
        case .Restricted:
            showAlert("Tu dispositivo no puede autorizar el uso de localización.")
            
        case .Denied:
            
            let alertController = UIAlertController(title: "Error", message: "Has denegado el permiso para utilizar tu localización. Puedes cambiarlo en la configuración de tu iPhone.", preferredStyle: .Alert)
            
            alertController.addAction(UIAlertAction(title: "Configuración", style: .Default, handler: { (alert) -> Void in
                
                UIApplication.sharedApplication().openURL(NSURL(string:UIApplicationOpenSettingsURLString)!);
            }))
            
            alertController.addAction(UIAlertAction(title: "Cancelar", style: .Cancel, handler: nil))
            
            self.presentViewController(alertController, animated: true, completion: nil)
            
        case .AuthorizedAlways:
            locationManager.startUpdatingLocation()
            
        default:
            break
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UbicanosVC : UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    // MARK: - CollectionView data source
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return eventSections.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! LocalCollectionViewCell
        
        //cell.tittle.text = eventSections[indexPath.item] as? String
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath){
        
        if indexPath.row == 0 {
            
            mapView.camera = GMSCameraPosition(target: CLLocationCoordinate2D(latitude: -12.0871923, longitude:-76.9951559), zoom: 15, bearing: 5, viewingAngle: 0)
        
            
        } else {
            
            mapView.camera = GMSCameraPosition(target: CLLocationCoordinate2D(latitude: -12.0690213, longitude:-77.0013786), zoom: 15, bearing: 0, viewingAngle: 0)
            
        }
        
    }
    
}


extension UbicanosVC: CLLocationManagerDelegate {
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if self.lastLocation == nil {
            
            self.lastLocation = locations.last!
            
            print(self.lastLocation?.coordinate.latitude)
            print(self.lastLocation?.coordinate.longitude)
            
            
//            let currentLocation = Location(latitude: self.lastLocation!.coordinate.latitude, longitude: self.lastLocation!.coordinate.longitude)
//            NSUserDefaults.standardUserDefaults().setDouble(self.lastLocation!.coordinate.latitude, forKey: Location.LocationJSONKeys.latitude)
//            NSUserDefaults.standardUserDefaults().setDouble(self.lastLocation!.coordinate.longitude, forKey: Location.LocationJSONKeys.longitude)
//            NSUserDefaults.standardUserDefaults().synchronize()
//            
//            print(currentLocation.latitude)
//            print(currentLocation.longitude)
            
            //performSegueWithIdentifier("toSendInformation", sender: self)
            locationManager.stopUpdatingLocation()
        }
    }
}

var didTapMarker = false

//MARK: MAP
extension UbicanosVC: GMSMapViewDelegate{
    
    func mapView(mapView: GMSMapView, willMove gesture: Bool) {
        
        if didTapMarker {
            didTapMarker = false
        }
            
        else {
            //showEventDetailView = false
        }
    }
    
    func mapView(mapView: GMSMapView, didTapAtCoordinate coordinate: CLLocationCoordinate2D) {
        
    }
    
    
    func mapView(mapView: GMSMapView, didTapMarker marker: GMSMarker) -> Bool {
        
        if let _ = marker.userData {
            
            return false
        }
            
        else {
            
            mapView.animateWithCameraUpdate(GMSCameraUpdate.setTarget(marker.position))
            
            return true
        }
    }
    
}
