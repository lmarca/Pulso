//
//  LoginVC.swift
//  Pulso
//
//  Created by Procesos on 16/11/16.
//  Copyright © 2016 Luis Marca Zapata. All rights reserved.
//

import UIKit

class LoginVC: myActivity {
    
    @IBOutlet weak var txtDni: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        txtPassword.secureTextEntry = true
        
        btnLogin.addTarget(self, action: #selector(LoginVC.openMenu), forControlEvents: UIControlEvents.TouchUpInside)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "BackIcon"), style: .Plain, target: self, action: #selector(LoginVC.popToViewController))
        
        addDoneKeyboard(txtDni)
        // Do any additional setup after loading the view.
    }
    
    func popToViewController() {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func openMenu() {
        
        PulsoServices.login(txtDni.text!, password: txtPassword.text!) { (user, error) in
            
            if let error = error {
                self.showError(error)
            }else {
                
                NSOperationQueue.mainQueue().addOperationWithBlock(){
                    
                    if user.numDoc.characters.count == 0 {
                        self.showAlert("Credenciales Inválidas")
                    }else {
                        UserManager.sharedInstance.currentUser = user
                        UserManager.saveUser()
                        self.openApp()
                    }
                }
            }
        }
    }
    
    private func openApp() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateInitialViewController()!
        self.presentViewController(controller, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension LoginVC {
    
    func addDoneKeyboard(textfield:UITextField) {
        
        textfield.keyboardType = UIKeyboardType.NumberPad
        
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRectMake(0, 0, self.view.frame.size.width, 50))
        doneToolbar.barStyle = .Default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        let done = UIBarButtonItem(title: "Aceptar", style: .Plain, target: self, action: #selector(LoginVC.doneButtonAction))
        
        var items: [UIBarButtonItem] = []
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        textfield.inputAccessoryView = doneToolbar
    }
    
    func doneButtonAction() {
        
        if txtDni.isFirstResponder() {
            txtDni.resignFirstResponder()
        
        }
    }
}

extension LoginVC : UITextFieldDelegate {
    
    
    func animateTextField(textField: UITextField, up: Bool, withOffset offset:CGFloat)
    {
        let movementDistance : Int = -Int(offset) + 44
        let movementDuration : Double = 0.4
        let movement : Int = (up ? movementDistance : -movementDistance)
        UIView.beginAnimations("animateTextField", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration)
        self.view.frame = CGRectOffset(self.view.frame, 0, CGFloat(movement))
        UIView.commitAnimations()
    }
    
    func textFieldDidBeginEditing(textField: UITextField)
    {
        self.animateTextField(textField, up: true, withOffset: textField.frame.origin.y / 2)
    }
    
    func textFieldDidEndEditing(textField: UITextField)
    {
        self.animateTextField(textField, up: false, withOffset: textField.frame.origin.y / 2)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    
}
