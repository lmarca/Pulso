//
//  SugerenciasVC.swift
//  Nao
//
//  Created by Procesos on 17/11/16.
//  Copyright © 2016 peru. All rights reserved.
//

import UIKit

class SugerenciasVC: myActivity {
    
    @IBOutlet weak var txtSugerencias: UITextView!
    
    @IBOutlet weak var startOne: UIButton!
    @IBOutlet weak var startTwo: UIButton!
    @IBOutlet weak var startThree: UIButton!
    @IBOutlet weak var startFour: UIButton!
    
    var isShowing = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController!.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "Abadi MT Condensed Extra Bold", size: 17)!, NSForegroundColorAttributeName: UIColor.whiteColor()]
        self.navigationItem.title = "PulsoSalud"

        let menuButton = UIBarButtonItem(image: UIImage(named: "BackIcon")?.imageWithRenderingMode(.AlwaysOriginal), style: .Plain, target: revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)))
        navigationItem.leftBarButtonItem = menuButton
        
        customButtons()
        // Do any additional setup after loading the view.
    }
    
    private func customButtons() {
        
        startOne.setImage(UIImage(named: "start_on"), forState: .Selected)
        startOne.setImage(UIImage(named: "start_off"), forState: .Normal)
        startTwo.setImage(UIImage(named: "start_on"), forState: .Selected)
        startTwo.setImage(UIImage(named: "start_off"), forState: .Normal)
        startThree.setImage(UIImage(named: "start_on"), forState: .Selected)
        startThree.setImage(UIImage(named: "start_off"), forState: .Normal)
        startFour.setImage(UIImage(named: "start_on"), forState: .Selected)
        startFour.setImage(UIImage(named: "start_off"), forState: .Normal)
    }
    
    @IBAction func onClickInput(sender: AnyObject) {
        
        switch sender .tag {
        case 0:
            isShowing = "1"
            startOne.selected = true
            startTwo.selected = false
            startThree.selected = false
            startFour.selected = false
            break
        case 1:
            isShowing = "2"
            startOne.selected = true
            startTwo.selected = true
            startThree.selected = false
            startFour.selected = false
            break
        case 2:
            isShowing = "3"
            startOne.selected = true
            startTwo.selected = true
            startThree.selected = true
            startFour.selected = false
            break
        case 3:
            isShowing = "4"
            startOne.selected = true
            startTwo.selected = true
            startThree.selected = true
            startFour.selected = true
            break
        default:
            break
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func cleanInput() {
        txtSugerencias.text = ""
        isShowing = "0"
        startOne.selected = false
        startTwo.selected = false
        startThree.selected = false
        startFour.selected = false
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SugerenciasVC: UITextViewDelegate {
    
    func textViewDidEndEditing(textView: UITextView) {
        
        if !textView.text.isEmpty {
            PulsoServices.sendRanking(isShowing, content: txtSugerencias.text!, completionHandler: { (msg, error) in
                
                NSOperationQueue.mainQueue().addOperationWithBlock(){
                    
                    if let error = error {
                        self.showError(error)
                    }else{
                        self.showAlert(msg)
                        self.cleanInput()
                    }
                }
                
            })
        }
        
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}
