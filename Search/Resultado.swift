//
//  Resultado.swift
//  Pulso
//
//  Created by Procesos on 8/12/16.
//  Copyright © 2016 Luis Marca Zapata. All rights reserved.
//

import UIKit

class Resultado: NSObject {
    
    struct LocationJSONKeys {
        static let latitude = "lat"
        static let longitude = "lng"
        static let zoom = "zoom"
    }
    
    var name: String!
    var status: String!
    
    init(name: String, status: String) {
        
        self.name = name
        self.status = status
    }
    
}
