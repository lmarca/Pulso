//
//  myActivity.swift
//  Pulso
//
//  Created by Procesos on 16/11/16.
//  Copyright © 2016 Luis Marca Zapata. All rights reserved.
//

import UIKit

class myActivity: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        


        // Do any additional setup after loading the view.
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    
    
    
    
    
    func formatButton(button: UIButton) {
        
        button.layer.cornerRadius = 25.0
        button.layer.masksToBounds = true
        button.layer.borderWidth = 1.0
        button.layer.borderColor = UIColor.whiteColor().CGColor
    }

}
