//
//  Filiacion02VC.swift
//  Pulso
//
//  Created by Procesos on 17/11/16.
//  Copyright © 2016 Luis Marca Zapata. All rights reserved.
//

import UIKit

class Filiacion02VC: myActivity {
    
    @IBOutlet weak var txtTelefono: UITextField!
    @IBOutlet weak var txtDireccion: UITextField!
    
    @IBOutlet weak var btnAddress: UIButton!
    @IBOutlet weak var btnInstruccion: UIButton!
    @IBOutlet weak var btnEstadoCivil: UIButton!
    
    
    var picker  : GKActionSheetPicker!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "BackIcon"), style: .Plain, target: self, action: #selector(Filiacion02VC.popToViewController))

        addDoneKeyboard(txtTelefono)
        // Do any additional setup after loading the view.
    }
    
    func popToViewController() {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func onClickEvent(sender: AnyObject) {
        
        switch sender .tag {
        case 0:
            chooseStateSource()
            break
        case 1:
            chooseTypeSource()
            break
        case 2:
            chooseAddressSource()
            break
        default:
            break
        }
    }
    
    
    func chooseAddressSource() {
        picker = GKActionSheetPicker.stringPickerWithItems(["El Agustino", "San Juan Lurigancho", "Comas", "Los Olivos", "El Rimac" , "Miraflores", "San Isidro", "Surco", "Barranco", "Pueblo Libre"], selectCallback: { (selected) in
            
            self.btnAddress.setTitle((selected as? String)!, forState: .Normal)
            
            }, cancelCallback: nil)
        picker .presentPickerOnView(self.view)
    }
    
    func chooseTypeSource() {
        picker = GKActionSheetPicker.stringPickerWithItems(["Analfabeto", "Primaria Incompleta", "Primaria Completa", "Secundaria Incompleta", "Secundaria Completa" , "Universitario Incompleto", "Universitario Completo", "Técnico Incompleto", "Técnico Completo", "Posgrado"], selectCallback: { (selected) in
            
            self.btnInstruccion.setTitle((selected as? String)!, forState: .Normal)
            
            }, cancelCallback: nil)
        picker .presentPickerOnView(self.view)
    }
    
    
    func chooseStateSource() {
        
        picker = GKActionSheetPicker.stringPickerWithItems(["Soltero", "Casado", "Divorciado", "Viudo", "Conviviente"], selectCallback: { (selected) in
            
            self.btnEstadoCivil.setTitle((selected as? String)!, forState: .Normal)
            
            }, cancelCallback: nil)
        picker .presentPickerOnView(self.view)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


extension Filiacion02VC : UITextFieldDelegate {
    
    
    func animateTextField(textField: UITextField, up: Bool, withOffset offset:CGFloat)
    {
        let movementDistance : Int = -Int(offset)
        let movementDuration : Double = 0.4
        let movement : Int = (up ? movementDistance : -movementDistance)
        UIView.beginAnimations("animateTextField", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration)
        self.view.frame = CGRectOffset(self.view.frame, 0, CGFloat(movement))
        UIView.commitAnimations()
    }
    
    func textFieldDidBeginEditing(textField: UITextField)
    {
        self.animateTextField(textField, up: true, withOffset: textField.frame.origin.y / 2)
    }
    
    func textFieldDidEndEditing(textField: UITextField)
    {
        self.animateTextField(textField, up: false, withOffset: textField.frame.origin.y / 2)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    
}

extension Filiacion02VC {
    
    func addDoneKeyboard(textfield:UITextField) {
        
        textfield.keyboardType = UIKeyboardType.NumberPad
        
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRectMake(0, 0, self.view.frame.size.width, 50))
        doneToolbar.barStyle = .Default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        let done = UIBarButtonItem(title: "Aceptar", style: .Plain, target: self, action: #selector(LoginVC.doneButtonAction))
        
        var items: [UIBarButtonItem] = []
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        textfield.inputAccessoryView = doneToolbar
    }
    
    func doneButtonAction() {
        
        if txtTelefono.isFirstResponder() {
            txtTelefono.resignFirstResponder()
            
        }
    }
}
