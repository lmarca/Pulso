//
//  ListResultadosVC.swift
//  Nao
//
//  Created by Procesos on 17/11/16.
//  Copyright © 2016 peru. All rights reserved.
//

import UIKit

class ListResultadosVC: myActivity {
    
    var isDismiss   = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController!.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "Abadi MT Condensed Extra Bold", size: 17)!, NSForegroundColorAttributeName: UIColor.whiteColor()]
        
        self.navigationItem.title = "PulsoSalud"
        
        if !isDismiss {
            let menuButton = UIBarButtonItem(image: UIImage(named: "BackIcon")?.imageWithRenderingMode(.AlwaysOriginal), style: .Plain, target: revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)))
            navigationItem.leftBarButtonItem = menuButton
        }else {
            let menuButton = UIBarButtonItem(image: UIImage(named: "BackIcon")?.imageWithRenderingMode(.AlwaysOriginal), style: .Plain, target: self, action: #selector(ListResultadosVC.popToViewController))
            navigationItem.leftBarButtonItem = menuButton
        }

        // Do any additional setup after loading the view.
    }
    
    func popToViewController() {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

