//
//  Filiacion01VC.swift
//  Pulso
//
//  Created by Procesos on 17/11/16.
//  Copyright © 2016 Luis Marca Zapata. All rights reserved.
//

import UIKit

class Filiacion01VC: myActivity {
    
    @IBOutlet weak var txtNombres: UITextField!
    @IBOutlet weak var buttonDoc: UIButton!
    @IBOutlet weak var buttonSex: UIButton!
    @IBOutlet weak var buttonDate: UIButton!
    
    var picker  : GKActionSheetPicker!
    var isMenuTo   = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if !isMenuTo {
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "BackIcon"), style: .Plain, target: self, action: #selector(Filiacion02VC.popToViewController))
        }else {
            let menuButton = UIBarButtonItem(image: UIImage(named: "BackIcon")?.imageWithRenderingMode(.AlwaysOriginal), style: .Plain, target: revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)))
            navigationItem.leftBarButtonItem = menuButton
        }
        

        // Do any additional setup after loading the view.
    }
    
    @IBAction func onClickEvent(sender: AnyObject) {
        
        switch sender .tag {
        case 0:
            chooseDocSource()
            break
        case 1:
            chooseSexSource()
            break
        case 2:
            chooseDateSource()
            break
        default:
            break
        }
    }
    
    func popToViewController() {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func chooseSexSource() {
        picker = GKActionSheetPicker.stringPickerWithItems(["Masculino","Femenino"], selectCallback: { (selected) in
            
            self.buttonSex.setTitle((selected as? String)!, forState: .Normal)
            
            }, cancelCallback: nil)
        picker .presentPickerOnView(self.view)
    }
    
    func chooseDocSource() {
        picker = GKActionSheetPicker.stringPickerWithItems(["D.N.I","Carnet de Extranjería", "Pasaporte"], selectCallback: { (selected) in
            
            self.buttonDoc.setTitle((selected as? String)!, forState: .Normal)
            
            }, cancelCallback: nil)
        picker .presentPickerOnView(self.view)
    }
    
    func chooseDateSource(){
        
        picker = GKActionSheetPicker.datePickerWithMode(.Date, from: nil, to: NSDate(), interval: 60*60*24, selectCallback: { (selected) in
            
            let formatter = NSDateFormatter()
            formatter.dateFormat = "dd-MM-yyyy"
            
            self.buttonDate.setTitle(formatter.stringFromDate((selected as? NSDate)!), forState: .Normal)
            
            }, cancelCallback: nil)
        
        picker.presentPickerOnView(self.view)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension Filiacion01VC : UITextFieldDelegate {
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField .resignFirstResponder()
        return false
    }
}
