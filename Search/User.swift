//
//  User
//  Culture
//
//  Created by J on 3/17/16.
//  Copyright © 2016 LIMAAPP E.I.R.L. All rights reserved.
//

import Foundation

class User: NSObject, NSCoding {
    

    var tipoDoc: String!
    var numDoc : String!
    var apellidos : String!
    var nombres: String!
    var sexo: String!
    var fecNac: String!
    var telefono: String!
    var celular: String!
    var estadoCivil: String!
    var gradoInst: String!
    var correo: String!
    var residenciaDistrito: String!
    var residenciaDireccion: String!
    var alredyInvited = false
    
    static let didLoginNotification = "UserDidLogin"
    static let didLogoutNotification = "UserDidLogout"
    static let didUpdateCurrentUserNotification = "UserDidUpdateCurrentUser"

    static let DocumentsDirectory = NSFileManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.URLByAppendingPathComponent("User")

    
    struct UserJSONKeys {
        static let Tipo_doc = "Tipo_doc"
        static let Num_doc = "Num_doc"
        static let Apellidos = "Apellidos"
        static let Nombres = "Nombres"
        static let Sexo = "Sexo"
        static let Fec_nac = "Fec_nac"
        static let Telefono = "Telefono"
        static let Celular = "Celular"
        static let Estado_civil = "Estado_civil"
        static let Grado_inst = "Grado_inst"
        static let Residencia_distrito = "Residencia_distrito"
        static let Residencia_direccion = "Residencia_direccion"
    }
    
    init(jsonObject: [String : AnyObject ]) {

        
        if let tipoDoc = jsonObject[UserJSONKeys.Tipo_doc] as? String {
            self.tipoDoc = tipoDoc
        }
        
        if let numDoc = jsonObject[UserJSONKeys.Num_doc] as? String {
            self.numDoc = numDoc
        }else{
            self.numDoc = ""
        }
        
        if let apellidos = jsonObject[UserJSONKeys.Apellidos] as? String {
            self.apellidos = apellidos
        }
        
        if let nombres = jsonObject[UserJSONKeys.Nombres] as? String {
            self.nombres = nombres
        }
        
        if let sexo = jsonObject[UserJSONKeys.Sexo] as? String {
            self.sexo = sexo
        }
        
        if let fecNac = jsonObject[UserJSONKeys.Fec_nac] as? String {
            self.fecNac = fecNac
        }
        
        if let telefono = jsonObject[UserJSONKeys.Telefono] as? String {
            self.telefono = telefono
        }
        
        if let celular = jsonObject[UserJSONKeys.Celular] as? String {
            self.celular = celular
        }
        
        if let estadoCivil = jsonObject[UserJSONKeys.Estado_civil] as? String {
            self.estadoCivil = estadoCivil
        }
        
        if let gradoInst = jsonObject[UserJSONKeys.Grado_inst] as? String {
            self.gradoInst = gradoInst
        }
        
        if let residenciaDistrito = jsonObject[UserJSONKeys.Residencia_distrito] as? String {
            self.residenciaDistrito = residenciaDistrito
        }
        
        if let residenciaDireccion = jsonObject[UserJSONKeys.Residencia_direccion] as? String {
            self.residenciaDireccion = residenciaDireccion
        }
        
    }
    
    // MARK: NSCoding
    
    func encodeWithCoder(aCoder: NSCoder) {
        
        if tipoDoc != nil {
            aCoder.encodeObject(tipoDoc, forKey: UserJSONKeys.Tipo_doc)
        }
        
        if numDoc != nil {
            aCoder.encodeObject(numDoc, forKey: UserJSONKeys.Num_doc)
        }
        if apellidos != nil {
            aCoder.encodeObject(tipoDoc, forKey: UserJSONKeys.Tipo_doc)
        }
        
        if nombres != nil {
            aCoder.encodeObject(nombres, forKey: UserJSONKeys.Nombres)
        }
        if apellidos != nil {
            aCoder.encodeObject(apellidos, forKey: UserJSONKeys.Apellidos)
        }
        
        if sexo != nil {
            aCoder.encodeObject(sexo, forKey: UserJSONKeys.Sexo)
        }
        if fecNac != nil {
            aCoder.encodeObject(fecNac, forKey: UserJSONKeys.Fec_nac)
        }
        
        if telefono != nil {
            aCoder.encodeObject(telefono, forKey: UserJSONKeys.Telefono)
        }
        
        if celular != nil {
            aCoder.encodeObject(celular, forKey: UserJSONKeys.Celular)
        }
        
        if estadoCivil != nil {
            aCoder.encodeObject(estadoCivil, forKey: UserJSONKeys.Estado_civil)
        }
        if gradoInst != nil {
            aCoder.encodeObject(gradoInst, forKey: UserJSONKeys.Grado_inst)
        }
        
        if residenciaDireccion != nil {
            aCoder.encodeObject(residenciaDireccion, forKey: UserJSONKeys.Residencia_direccion)
        }
        
        if residenciaDistrito != nil {
            aCoder.encodeObject(residenciaDistrito, forKey: UserJSONKeys.Residencia_distrito)
        }
    

        
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init()
        
        if let tipoDoc = aDecoder.decodeObjectForKey(UserJSONKeys.Tipo_doc) as? String {
            self.tipoDoc = tipoDoc
        }
        
        if let numDoc = aDecoder.decodeObjectForKey(UserJSONKeys.Num_doc) as? String {
            self.numDoc = numDoc
        }
        
        if let apellidos = aDecoder.decodeObjectForKey(UserJSONKeys.Apellidos) as? String {
            self.apellidos = apellidos
        }
        
        if let nombres = aDecoder.decodeObjectForKey(UserJSONKeys.Nombres) as? String {
            self.nombres = nombres
        }
        
        if let sexo = aDecoder.decodeObjectForKey(UserJSONKeys.Sexo) as? String {
            self.sexo = sexo
        }
        
        if let fecNac = aDecoder.decodeObjectForKey(UserJSONKeys.Fec_nac) as? String {
            self.fecNac = fecNac
        }
        
        if let telefono = aDecoder.decodeObjectForKey(UserJSONKeys.Telefono) as? String {
            self.telefono = telefono
        }
        
        if let celular = aDecoder.decodeObjectForKey(UserJSONKeys.Celular) as? String {
            self.celular = celular
        }
        if let estadoCivil = aDecoder.decodeObjectForKey(UserJSONKeys.Estado_civil) as? String {
            self.estadoCivil = estadoCivil
        }
        
        if let gradoInst = aDecoder.decodeObjectForKey(UserJSONKeys.Grado_inst) as? String {
            self.gradoInst = gradoInst
        }
        
        if let residenciaDireccion = aDecoder.decodeObjectForKey(UserJSONKeys.Residencia_direccion) as? String {
            self.residenciaDireccion = residenciaDireccion
        }
        
        if let residenciaDistrito = aDecoder.decodeObjectForKey(UserJSONKeys.Residencia_distrito) as? String {
            self.residenciaDistrito = residenciaDistrito
        }
    }
    
 
}

extension User {
    
    func displayName() -> String {
        
        return self.nombres + " " + self.apellidos
    }
}
