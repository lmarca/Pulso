//
//  MenuViewController.swift
//  Culture
//
//  Created by J on 3/23/16.
//  Copyright © 2016 LIMAAPP E.I.R.L. All rights reserved.
//

import UIKit

let inicioTitle = "Inicio"
let filiacionTitle = "Filiación"
let resultadosTitle =  "Resultados"
let riesgosTitle = "Riesgos de Salud"
let ubicanosTitle = "Ubícanos"
let sugerenciasTitle = "Sugerencias"

class MenuViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var profileImageView: ProfileImageView!
    @IBOutlet weak var profileName: UILabel!
    
    var previouslyPresentedRow = 0
    
    var homeViewController: UINavigationController?
    var filiacionViewController: UINavigationController?
    var resultadosViewController: UINavigationController?
    var riesgosViewController: UINavigationController?
    var ubicanosViewController: UINavigationController?
    var sugerenciasViewController: UINavigationController?
    
    var menuItemTitles = [inicioTitle, filiacionTitle, resultadosTitle, riesgosTitle, ubicanosTitle, sugerenciasTitle]

    override func viewWillAppear(animated: Bool) {
        changeProfile()
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        //tableView.rowHeight = UITableViewAutomaticDimension
        //tableView.estimatedRowHeight = 70
        changeProfile()
    }
    
    func changeProfile() {
        
        profileName.text = "Eliana Goya"
        profileImageView.image = readImage()//NonLoggedUser
        menuItemTitles = [inicioTitle, filiacionTitle, resultadosTitle, riesgosTitle, ubicanosTitle, sugerenciasTitle]
        
        self.tableView.reloadData()
    }

    //MARK: Log Out - Log In
    
    func logIn() {
        changeProfile()
        
    }
    
    func logOut() {
        changeProfile()
    }
    
    @IBAction func onClickEvent(sender: AnyObject) {
        
        let alertController = UIAlertController(title: nil, message: "Seguro deseas salir?", preferredStyle: .ActionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancelar", style: .Cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        let destroyAction = UIAlertAction(title: "Salir", style: .Destructive) { (action) in
            
            NSOperationQueue.mainQueue().addOperationWithBlock({
                UserManager.deleteUser()
                self.openLoginScene()
            })
        }
        
        alertController.addAction(destroyAction)
        
        self.presentViewController(alertController, animated:true, completion: nil)
    }
    
    
    func openLoginScene() {
        let loginStoryboard = UIStoryboard(name: "Login", bundle: nil)
        let loginViewController = loginStoryboard.instantiateViewControllerWithIdentifier("ViewController") as! ViewController
        let navigationViewController = loginStoryboard.instantiateViewControllerWithIdentifier("LoginNavController") as! TransparentNavigationController
        navigationViewController.setViewControllers([loginViewController], animated: true)
        self.showViewController(navigationViewController, sender: self)
    }
    
  
}

extension MenuViewController {
    
    func readImage() -> UIImage {
        
        let filePath = getDocumentsDirectory().URLByAppendingPathComponent("UserIcon.png")?.path
        if NSFileManager.defaultManager().fileExistsAtPath(filePath!) {
            return UIImage(contentsOfFile: filePath!)!
        }
        
        return UIImage()
    }
    
    func getDocumentsDirectory() -> NSURL {
        
        let paths = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
}

extension MenuViewController: UITableViewDataSource {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItemTitles.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let title = menuItemTitles[indexPath.row]
        let cell = tableView.dequeueReusableCellWithIdentifier("MenuItemCell", forIndexPath: indexPath) as! MenuItemCell
        cell.menuItemTitle.text = title
        
        cell.selectionStyle = .None
        
        cell.setNeedsLayout()
        return cell
    }
}

extension MenuViewController: UITableViewDelegate {
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
     
        let previouslyPresentedCell = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: previouslyPresentedRow, inSection: 0))
            previouslyPresentedCell?.setSelected(false, animated: false)
        
        let currentCell = tableView.cellForRowAtIndexPath(indexPath)
        currentCell!.setSelected(true, animated: false)

        var newFrontController : UIViewController!

        let title = menuItemTitles[indexPath.row]
        
        
        //Inicio
        
        if title == inicioTitle {
            
            let nav = self.storyboard?.instantiateViewControllerWithIdentifier("HomeNavController") as! TransparentNavigationController
            let locationViewController = self.storyboard?.instantiateViewControllerWithIdentifier("HomeVC") as! HomeVC
            nav.setViewControllers([locationViewController], animated: true)
            self.homeViewController = nav
            newFrontController = self.homeViewController
            
            /*
            if let locationViewController = self.homeViewController {
                newFrontController = locationViewController
            }
            
            else {
                let nav = self.storyboard?.instantiateViewControllerWithIdentifier("HomeNavController") as! TransparentNavigationController
                let locationViewController = self.storyboard?.instantiateViewControllerWithIdentifier("HomeVC") as! HomeVC
                nav.setViewControllers([locationViewController], animated: true)
                self.homeViewController = nav
                newFrontController = self.homeViewController
            }*/
        }
            
        //Filiacion
            
        else if title == filiacionTitle {
            
            if let locationViewController = self.filiacionViewController {
                newFrontController = locationViewController
            }
                
            else {
                
                let mainStoryBoard = UIStoryboard(name: "Login", bundle: nil)
                let nav = mainStoryBoard.instantiateViewControllerWithIdentifier("AfiliacionNavController") as! TransparentNavigationController
                let filiacionViewController = mainStoryBoard.instantiateViewControllerWithIdentifier("Filiacion01VC") as! Filiacion01VC
                filiacionViewController.isMenuTo = true
                nav.setViewControllers([filiacionViewController], animated: true)
                self.filiacionViewController = nav
                newFrontController = self.filiacionViewController
            }
        }

        //Resultados
            
        else if title == resultadosTitle {
            
            if let locationViewController = self.resultadosViewController {
                newFrontController = locationViewController
            }
                
            else {
                let nav = self.storyboard?.instantiateViewControllerWithIdentifier("ResultadosNavController") as! BlackTransparentNavigationController
                let locationViewController = self.storyboard?.instantiateViewControllerWithIdentifier("ListResultadosVC") as! ListResultadosVC
                nav.setViewControllers([locationViewController], animated: true)
                self.resultadosViewController = nav
                newFrontController = self.resultadosViewController
            }
        }
        
        //Riesgos
            
        else if title == riesgosTitle {
            
            if let locationViewController = self.riesgosViewController {
                newFrontController = locationViewController
            }
                
            else {
                let nav = self.storyboard?.instantiateViewControllerWithIdentifier("HomeNavController") as! TransparentNavigationController
                let locationViewController = self.storyboard?.instantiateViewControllerWithIdentifier("HomeVC") as! HomeVC
                nav.setViewControllers([locationViewController], animated: true)
                self.riesgosViewController = nav
                newFrontController = self.riesgosViewController
            }
        }
        
        //Ubicanos
            
        else if title == ubicanosTitle {
            
            if let locationViewController = self.ubicanosViewController {
                newFrontController = locationViewController
            }
                
            else {
                let nav = self.storyboard?.instantiateViewControllerWithIdentifier("UbicanosNavController") as! BlackTransparentNavigationController
                let locationViewController = self.storyboard?.instantiateViewControllerWithIdentifier("UbicanosVC") as! UbicanosVC
                nav.setViewControllers([locationViewController], animated: true)
                self.ubicanosViewController = nav
                newFrontController = self.ubicanosViewController
            }
        }
            
        //Sugerencias
        
        else if title == sugerenciasTitle {
            
            if let locationViewController = self.sugerenciasViewController {
                newFrontController = locationViewController
            }
                
            else {
                let nav = self.storyboard?.instantiateViewControllerWithIdentifier("SugerenciasNavController") as! BlackTransparentNavigationController
                let locationViewController = self.storyboard?.instantiateViewControllerWithIdentifier("SugerenciasVC") as! SugerenciasVC
                nav.setViewControllers([locationViewController], animated: true)
                self.sugerenciasViewController = nav
                newFrontController = self.sugerenciasViewController
            }
        }

        revealViewController().pushFrontViewController(newFrontController, animated: true)

        previouslyPresentedRow = indexPath.row
        
    }
 
}


